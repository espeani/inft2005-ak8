![pipeline](https://gitlab.stud.idi.ntnu.no/espeani/inft2005-ak8//badges/master/pipeline.svg)

# CI/CD testing med Cypress.

Dette er et prosjekt som er ment for å teste ut CI/CD med Cypress.

Original koden er hentet fra [Nils Tesdal/cypress-bdd-start](https://gitlab.stud.idi.ntnu.no/nilstesd/cypress-bdd-start)
