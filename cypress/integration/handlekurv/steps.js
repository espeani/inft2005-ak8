import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#list').should('contain', '4 Hubba bubba');
    cy.get('#list').should('contain', '5 Smørbukk');
    cy.get('#list').should('contain', '1 Stratos');
    cy.get('#list').should('contain', '2 Hobby');
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer$/, () => {
    cy.get('#emptyCart').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#list').should('not.contain', 'Hubba bubba');
    cy.get('#list').should('not.contain', 'Smørbukk');
    cy.get('#list').should('not.contain', 'Stratos');
    cy.get('#list').should('not.contain', 'Hobby');
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#list').should('contain', '1 Hubba bubba');
});
