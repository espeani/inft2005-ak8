import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit('http://localhost:8080');

  cy.get('#product').select('Hubba bubba');
  cy.get('#quantity').clear().type('4');
  cy.get('#saveItem').click();

  cy.get('#product').select('Smørbukk');
  cy.get('#quantity').clear().type('5');
  cy.get('#saveItem').click();

  cy.get('#product').select('Stratos');
  cy.get('#quantity').clear().type('1');
  cy.get('#saveItem').click();

  cy.get('#product').select('Hobby');
  cy.get('#quantity').clear().type('2');
  cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
  cy.get('#fullName').type('Ola Nordmann');
  cy.get('#address').type('Osloveien 82');
  cy.get('#postCode').type('0270');
  cy.get('#city').type('Oslo');
  cy.get('#creditCardNo').type('1234567890123456');
});

And(/^trykker på Fullfør kjøp$/, () => {
  cy.get('#submit').click(); // Jeg la til ID som manglet i formen.
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
  cy.get('.confirmation').should('contain.text', 'Din ordre er registrert.')
});

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit('http://localhost:8080');

  cy.get('#product').select('Hubba bubba');
  cy.get('#quantity').clear().type('4');
  cy.get('#saveItem').click();

  cy.get('#product').select('Smørbukk');
  cy.get('#quantity').clear().type('5');
  cy.get('#saveItem').click();

  cy.get('#product').select('Stratos');
  cy.get('#quantity').clear().type('1');
  cy.get('#saveItem').click();

  cy.get('#product').select('Hobby');
  cy.get('#quantity').clear().type('2');
  cy.get('#saveItem').click();
});

And(/^jeg trykker på Gå til betaling$/, () => {
  cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get('#fullName').clear();
  cy.get('#address').clear();
  cy.get('#postCode').clear();
  cy.get('#city').clear();
  cy.get('#creditCardNo').clear();
  cy.get('#fullName').clear(); // Måtte ha denne for at Cypress skulle se feilmeldingen til creditCardNo
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get('#fullNameError').should('have.text', 'Feltet må ha en verdi')
  cy.get('#addressError').should('have.text', 'Feltet må ha en verdi')
  cy.get('#postCodeError').should('have.text', 'Feltet må ha en verdi')
  cy.get('#cityError').should('have.text', 'Feltet må ha en verdi')
  cy.get('#creditCardNoError').should('have.text', 'Kredittkortnummeret må bestå av 16 siffer')
});